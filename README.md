#MotionDetectorGreeter
=====================
This is simple app which plays sound and takes pictures from your default camera.

It uses [Webcam Capture API](http://webcam-capture.sarxos.pl/)
 

It takes two parameters from command line:
1. -file path_to_file where your .wav was placed.
2. -images path_to_images_dir where your images should be stored, otherwise current dir is used.