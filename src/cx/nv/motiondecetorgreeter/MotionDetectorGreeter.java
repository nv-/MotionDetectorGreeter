package cx.nv.motiondecetorgreeter;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionDetector;
import com.github.sarxos.webcam.WebcamMotionListener;
import java.awt.Dimension;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


/**
 * @author Rafał Rutkowski
 */
public class MotionDetectorGreeter extends MotionDetectorBase{
	private static final int SLEEP_TIME = 2000;
	private static volatile boolean alreadyGreeted;
	
	
	public static void setIsAlreadyGreeted( boolean status ){
		alreadyGreeted = status;
	}

	public static boolean getIsAlreadyGreeted(){
		return alreadyGreeted;
	}
	
	private static Webcam getInitializedWebcam(){
		Webcam webcam = Webcam.getDefault();
		
		//we set max dimension
		Dimension[] dimensions = webcam.getViewSizes();
		Dimension dimension = dimensions[ dimensions.length - 1 ];
		
		webcam.setViewSize( dimension );
		
		return webcam;
	}
	
	private static void initMotionDetector( Webcam webcam, WebcamMotionListener[] webcamMotionListeners ){
		WebcamMotionDetector motionDetector = new WebcamMotionDetector( webcam );
		
		motionDetector.setInterval( SLEEP_TIME );
		
		for( int i = 0; i < webcamMotionListeners.length; i++ ){
			motionDetector.addMotionListener( webcamMotionListeners[ i ] );
		}
		
		motionDetector.start();
	}

	/**
	 * Method checks that parameter exists.
	 * 
	 * @param args Command line arguments
	 * @param param Param to look for in arguments
	 * @return
	 */
	protected static boolean getHasParameter( String[] args, String param ){
		return Arrays.asList( args ).contains( param );
	}

	/**
	 * Method for getting value of command line parameter
	 * 
	 * @param args Command line arguments
	 * @param param Param to look value for
	 * @return Param value or throws exception if not found
	 * @throws ArrayIndexOutOfBoundsException
	 */
	protected static String getParameterValue( String[] args, String param ) throws ArrayIndexOutOfBoundsException{
		List listedArray = Arrays.asList( args );
		int index = listedArray.indexOf( param );

		if( ( listedArray.size() - 1 ) < index ){
			throw new ArrayIndexOutOfBoundsException( "Have you forgot about path parameter?" );
		}

		return listedArray.get( index + 1 ).toString();
	}
	
	
	/**
	 * Should I even explain what is main function?
	 * 
	 * @param args the command line arguments
	 * @throws java.io.IOException
	 * @throws java.lang.InterruptedException
	 */
	public static void main( String[] args ) throws IOException, InterruptedException{
		Webcam webcam = getInitializedWebcam();
		
		String fileParameter = "-file";
		String imagesParameter = "-images";
		String imagesPath = ".";
		
		if( getHasParameter( args, imagesParameter ) ){
			imagesPath = getParameterValue( args, imagesParameter );
			
			logInfoInternal( "Images path would be:" + imagesPath );
		}

		if( getHasParameter( args, fileParameter ) ){
			String filePath = getParameterValue( args, fileParameter );
			
			WebcamMotionListener motionDetectorAudioPlayer = new MotionDetectorAudioPlayer( webcam, filePath );
			WebcamMotionListener motionDetectorImageCapturer = new MotionDetectorImageCapturer( webcam, imagesPath );
			WebcamMotionListener[] webcamMotionListeners = { motionDetectorAudioPlayer, motionDetectorImageCapturer };
			
			initMotionDetector( webcam, webcamMotionListeners );
			
			logInfoInternal( "Initialization completed, waiting for prey..." );
			
			while( !getIsAlreadyGreeted() ){
				Thread.sleep( SLEEP_TIME );
			}
			
			logInfoInternal( "There is nothing more to be done. Exiting" );
		}else{
			System.out.println( "This app requires -file {file_path} parameter." );
		}
	}
}
