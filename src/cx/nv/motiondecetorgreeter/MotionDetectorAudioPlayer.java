package cx.nv.motiondecetorgreeter;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


/**
 * Class for playing audio when motion is discovered.
 * 
 * @author Rafał Rutkowski
 */
public class MotionDetectorAudioPlayer extends MotionDetectorListenerBase{
	private static boolean isAudioLocked;
	
	private String filePath;
	
	
	public MotionDetectorAudioPlayer( Webcam webcam, String filePath ){
		super( webcam );
		
		this.filePath = filePath;
	}
	
	public static boolean getIsAudioLocked(){
		return isAudioLocked;
	}

	public static void setIsAudioLocked( boolean status ){
		isAudioLocked = status;
	}

	@Override
	public void motionDetected( WebcamMotionEvent wme ){
		if( !getIsAudioLocked() ){
			setIsAudioLocked( true );

			try{
				this.handleAudioPlay();
				
				this.logInfo( "Playing file :)" );
			}catch( UnsupportedAudioFileException | IOException | LineUnavailableException | InterruptedException ex ){
				Logger.getLogger( MotionDetectorGreeter.class.getName() ).log( Level.SEVERE, null, ex );
			}
		}
	}
	
	private void handleAudioPlay() throws UnsupportedAudioFileException, IOException, LineUnavailableException, InterruptedException{
		File file = new File( this.filePath );

		AudioInputStream audioInputStream = AudioSystem.getAudioInputStream( file.getAbsoluteFile() );

		Clip clip = AudioSystem.getClip();

		clip.open( audioInputStream );

		clip.start();
		
		// we are creating line listener for notification when playback is done
		LineListener lineListener = new LineListener(){
			@Override
			public void update( LineEvent lineEvent ){
				if( lineEvent.getType() == LineEvent.Type.CLOSE || lineEvent.getType() == LineEvent.Type.STOP ){
					MotionDetectorGreeter.setIsAlreadyGreeted( true );
				}
			}
		};

		clip.addLineListener( lineListener );
	}
}
