package cx.nv.motiondecetorgreeter;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Base class which provides basic logging.
 * 
 * @author Rafał Rutkowski
 */
abstract class MotionDetectorBase{
	protected void logInfo( String text ){
		logInfoInternal( text, this.getClass().getSimpleName() );
	}
	
	protected static void logInfoInternal( String text ){
		logInfoInternal( text, MotionDetectorGreeter.class.getSimpleName() );
	}
	
	protected static void logInfoInternal( String text, String className ){
		Logger.getLogger( className ).log( Level.INFO, text );
	}
}
