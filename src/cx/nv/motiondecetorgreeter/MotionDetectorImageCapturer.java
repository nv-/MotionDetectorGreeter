package cx.nv.motiondecetorgreeter;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionEvent;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;


/**
 * Class for capturing images from webcam on motion discovered.
 * 
 * @author Rafał Rutkowski
 */
public class MotionDetectorImageCapturer extends MotionDetectorListenerBase{
	private String imagesDirPath;
	private DateFormat dateFormat;

	
	public MotionDetectorImageCapturer( Webcam webcam, String imagesDirPath ){
		super( webcam );

		this.imagesDirPath = imagesDirPath;
		
		this.dateFormat = new SimpleDateFormat( "YYYY-MM-dd_HH-dd-ss" );
	}

	@Override
	public void motionDetected( WebcamMotionEvent wme ){
		try{
			this.captureImage();

			this.logInfo( "Captured new Image!" );
		}catch( IOException ex ){
			Logger.getLogger( MotionDetectorGreeter.class.getName() ).log( Level.SEVERE, null, ex );
		}
	}

	/**
	 * Method captures Image from webcam and save its content to file.
	 *
	 * @throws IOException
	 */
	private void captureImage() throws IOException{
		ImageIO.write( this.getWebcam().getImage(), "PNG", new File( this.getCapturedImageFilePathWithName() ) );
	}
	
	/**
	 * @return FilePath with FileName
	 */
	private String getCapturedImageFilePathWithName(){
		Date date = new Date();

		return this.imagesDirPath + "/captured-" + this.dateFormat.format( date ) + ".png";
	}
}
