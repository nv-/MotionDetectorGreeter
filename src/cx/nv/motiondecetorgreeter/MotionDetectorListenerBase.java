package cx.nv.motiondecetorgreeter;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionListener;


/**
 * Base class for all of the listeners, gives access to webcam.
 * 
 * @author Rafał Rutkowski
 */
abstract public class MotionDetectorListenerBase extends MotionDetectorBase implements WebcamMotionListener{
	private Webcam webcam;
	
	
	public MotionDetectorListenerBase( Webcam webcam ){
		this.webcam = webcam;
	}

	protected Webcam getWebcam(){
		return webcam;
	}
}
